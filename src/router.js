import Vue from "vue";
import Router from "vue-router";
import firebase from "firebase";

import Home from "./views/Home";
import Forum from "./views/Forum";
import Matches from "./views/Matches";
import Login from "./views/Login";
import Oops from "./views/Oops";

Vue.use(Router);

const router = new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "*",
      redirect: "/login"
    },
    {
      path: "/",
      redirect: "/login"
    },
    {
      path: "/home",
      name: "home",
      component: Home,
      meta: {
        title: "Central Hub - Home",
        requiresAuth: true
      }
    },
    {
      path: "/matches",
      name: "Matches",
      component: Matches,
      meta: {
        title: "Central Hub - Match",
        requiresAuth: true
      }
    },
    {
      path: "/forum",
      name: "Forum",
      component: Forum,
      props: true,
      meta: {
        title: "Central Hub - Forum",
        requiresAuth: true
      }
    },
    {
      path: "/login",
      name: "Login",
      component: Login,
      meta: {
        title: "Central Hub - Login"
      }
    },
    {
      path: "/oops",
      name: "Oops",
      component: Oops,
      meta: {
        title: "Permission Denied",
        errorpage: true
      }
    }
  ]
});

router.beforeEach((to, from, next) => {
  //dynamic title display from route meta 'title'
  if (to.meta.title) {
    document.title = to.meta.title;
  } else {
    document.title = "Central Hub";
  }
  //auth based route propagation check
  const currentUser = firebase.auth().currentUser;
  const requiresAuth = to.matched.some(record => record.meta.requiresAuth);

  if (!currentUser && requiresAuth) {
    next("login");
  } else next();
});

export default router;
