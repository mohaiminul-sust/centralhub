import Vue from "vue";
import firebase from "firebase";
import VueProgressBar from "vue-progressbar";
import BootstrapVue from "bootstrap-vue";
import VueClipboard from "vue-clipboard2";
import Toasted from "vue-toasted";

import { library } from "@fortawesome/fontawesome-svg-core";
import { faThLarge } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";

import "bootstrap";
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue/dist/bootstrap-vue.css";

import App from "./App.vue";
import router from "./router";

Vue.config.productionTip = false;

Vue.use(BootstrapVue);
Vue.use(VueClipboard);
Vue.use(Toasted, {
  iconPack: "fontawesome",
  duration: 2500,
  theme: "outline"
});

library.add(faThLarge);
Vue.component("font-awesome-icon", FontAwesomeIcon);

const progressOptions = {
  color: "#ffffff",
  failedColor: "#FFA500",
  thickness: "10px",
  transition: {
    speed: "0.2s",
    opacity: "0.6s",
    termination: 300
  },
  autoRevert: true,
  location: "top",
  inverse: false
};
Vue.use(VueProgressBar, progressOptions);

let app = "";

var config = {
  apiKey: "AIzaSyAyCVgmKPapKoyDhaJ4bdQ2m4zuxhkK32Y",
  authDomain: "centralscrape.firebaseapp.com",
  databaseURL: "https://centralscrape.firebaseio.com",
  projectId: "centralscrape",
  storageBucket: "centralscrape.appspot.com",
  messagingSenderId: "55433293981"
};
firebase.initializeApp(config);

firebase.auth().onAuthStateChanged(() => {
  if (!app) {
    app = new Vue({
      router,
      render: h => h(App)
    }).$mount("#app");
  }
});
